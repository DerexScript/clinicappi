<?php
header("access-control-allow-credentials: true");
header("Access-Control-Allow-Origin: null");
header('Content-Type: application/json');

require "conexao.php";
$conexao = mysqlConnect();


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST["selecione_especialidade_medica"])) {
    $especialidade_selecio = $_POST["selecione_especialidade_medica"];
    $busca_nome_medico = $conexao->prepare("select medico.Especialidade, pessoa.nome, pessoa.idpessoa from (medico join pessoa on medico.Codigo_medico = pessoa.idpessoa) where medico.Especialidade=?");
    $busca_nome_medico->execute(array($especialidade_selecio));
    $retJson = [];
    if ($busca_nome_medico->rowCount() > 0) {
        $selectData = $busca_nome_medico->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($selectData);
    } else {
        echo json_encode(array("sucess" => "false", "Error" => "medico nao encontrado"));
    }
}

