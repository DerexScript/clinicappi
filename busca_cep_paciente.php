<?php
require "conexao.php";
$conexao = mysqlConnect();
function filtraEntrada($dado)
{
$dado = trim($dado);               // remove espaços no inicio e no final da string
$dado = stripslashes($dado);       // remove contra barras: "cobra d\'agua" vira "cobra d'agua"
$dado = htmlspecialchars($dado);   // caracteres especiais do HTML (como < e >) são codificados
return $dado;
}

$cep_func = "";

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["cep_paciente"])) {
$cep = $_POST["cep_paciente"];
$cep_paciente = filtraEntrada($_POST["cep_paciente"]);
$tabelaajax1 = $conexao->prepare("SELECT * FROM base_endereco_ajax WHERE cep = :cep LIMIT 1");
$tabelaajax1->bindValue(':cep', $cep_paciente);
$tabelaajax1->execute();
if ($tabelaajax1->rowCount() > 0) {
$endajax1 = $tabelaajax1->fetch(PDO::FETCH_ASSOC);
echo json_encode($endajax1);
} else {
echo json_encode(array("Error" => "CEP NOT FOUND"));
}
} else {
echo json_encode(array("Error" => "Invalid Request"));
http_response_code(500);
}
