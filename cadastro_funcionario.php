<?php
require "conexao.php";
$conexao=mysqlConnect();

function filtraEntrada($dado)
{
    $dado = trim($dado);               // remove espaços no inicio e no final da string
    $dado = stripslashes($dado);       // remove contra barras: "cobra d\'agua" vira "cobra d'agua"
    $dado = htmlspecialchars($dado);   // caracteres especiais do HTML (como < e >) são codificados

    return $dado;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nome_func = $email_func = $telefone_func = $cep_func = $logradouro_func = $bairro_func = $cidade_func = $estado_func = $data_inicio_func = $salario_func = $passfunc = $tipofunc = $crm_medico = $especialidade_medico = $idpessoa =$senha_func_hash= '';

    $nome_func = filtraEntrada($_POST["nome_func"]);
    $email_func = filtraEntrada($_POST["email_func"]);
    $telefone_func = filtraEntrada($_POST["telefone_func"]);
    $cep_func = filtraEntrada($_POST["cep_func"]);
    $logradouro_func = filtraEntrada($_POST["logradouro_func"]);
    $bairro_func = filtraEntrada($_POST["bairro_func"]);
    $cidade_func = filtraEntrada($_POST["cidade_func"]);
    $estado_func = filtraEntrada($_POST["estado_func"]);
    $data_inicio_func = filtraEntrada($_POST["data_inicio_func"]);
    $salario_func = filtraEntrada($_POST["salario_func"]);
    $passfunc = filtraEntrada($_POST["passfunc"]);
    $tipofunc = filtraEntrada($_POST["tipofunc"]);
    $crm_medico = filtraEntrada($_POST["crm_medico"]);
    $especialidade_medico = filtraEntrada($_POST["especialidade_medico"]);
    $senha_func_hash=password_hash($passfunc,PASSWORD_DEFAULT);




    //listar dados da tabla endereço



    //termina a listagem de endereços

    try {

        $query_novo_endereco = $conexao->prepare("insert into pessoa(nome,email,telefone,cep,Logradouro,Bairro,cidade,estado) values (?,?,?,?,?,?,?,?)");
        try {
            $conexao->beginTransaction();
            $query_novo_endereco->execute(array($nome_func, $email_func, $telefone_func, $cep_func, $logradouro_func, $bairro_func, $cidade_func, $estado_func));
            $idpessoa = $conexao->lastInsertId();
            $conexao->commit();

            #teste cadastro funcionario



        } catch (PDOException $erro) {
            $conexao->rollback();
            echo "erro cadastra pessoa" . $erro->getMessage();


        }
    } catch (PDOException $e) {
        echo "erro" . $e->getMessage();
    }


    #cadastrando o func
    $query_func = $conexao->prepare("insert into funcionario(DataContrato,salario,SenhaHash,Codigo_pessoa) values (?,?,?,?)");
    try {
        $conexao->beginTransaction();
        $query_func->execute(array($data_inicio_func, $salario_func, $senha_func_hash, $idpessoa));
        $conexao->commit();


    } catch (PDOException $erro) {
        $conexao->rollback();

    }
if ($tipofunc=="func_medico"){

    $medico= $conexao->prepare("insert into medico(Especialidade,crm,Codigo_medico) values (?,?,?)");
    try {
        $conexao->beginTransaction();
        $medico->execute(array($especialidade_medico, $crm_medico, $idpessoa));
        $conexao->commit();


    } catch (PDOException $erro) {
        $conexao->rollback();

    }

}

}

?>