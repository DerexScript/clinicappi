<?php
header("access-control-allow-credentials: true");
header("Access-Control-Allow-Origin: null");
header('Content-Type: application/json');

require "conexao.php";
$conexao = mysqlConnect();


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST["idMedico"]) && isset($_POST["chosenDate"])) {


    $stmt = $conexao->prepare("SELECT horario FROM agenda WHERE CodigoMedico = :codigoMedico AND data_agenda = :data_agenda");
    $stmt->bindValue(":codigoMedico", $_POST["idMedico"]);
    $stmt->bindValue(":data_agenda", $_POST["chosenDate"]);
    $stmt->execute();


    if ($stmt->rowCount() > 0) {
        $hoursArray = ["success" => true];
        $hData = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($hData as $key => $value){
            foreach ($value as $key1 => $value1){
                $hoursArray[] = ["hour" => $value1];
            }
        }
        echo json_encode($hoursArray);
        //echo json_encode(array("sucess" => "false", "Error" => "medico nao encontrado"));
    }else{
        $hoursArray = ["success" => false, "msg" => "não há agendamentos para esta data e medico"];
        echo json_encode($hoursArray);
    }
}

