<?php
require "conexao.php";
$conexao=mysqlConnect();

function filtraEntrada($dado)
{
    $dado = trim($dado);               // remove espaços no inicio e no final da string
    $dado = stripslashes($dado);       // remove contra barras: "cobra d\'agua" vira "cobra d'agua"
    $dado = htmlspecialchars($dado);   // caracteres especiais do HTML (como < e >) são codificados

    return $dado;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nome_paciente = $email_paciente = $telefone_paciente= $cep_paciente = $logradouro_paciente = $bairro_paciente = $cidade_paciente = $estado_estado_paciente = $tipo_sanguíneo=$paso_paciente=$altura_paciente=$idpessoa= '';

    $nome_paciente = filtraEntrada($_POST["nome_paciente"]);
    $email_paciente= filtraEntrada($_POST["email_paciente"]);
    $telefone_paciente = filtraEntrada($_POST["telefone_paciente"]);
    $cep_paciente = filtraEntrada($_POST["cep_paciente"]);
    $logradouro_paciente = filtraEntrada($_POST["logradouro_paciente"]);
    $bairro_paciente = filtraEntrada($_POST["bairro_paciente"]);
    $cidade_paciente = filtraEntrada($_POST["cidade_paciente"]);
    $estado_estado_paciente = filtraEntrada($_POST["estado_estado_paciente"]);
    $tipo_sanguíneo = filtraEntrada($_POST["tipo_sanguíneo"]);
    $paso_paciente = filtraEntrada($_POST["paso_paciente"]);
    $altura_paciente = filtraEntrada($_POST["altura_paciente"]);

    try {


        $query_novo_pessoa = $conexao->prepare("insert into pessoa(nome,email,telefone,cep,Logradouro,Bairro,cidade,estado) values (?,?,?,?,?,?,?,?)");
        try {
            $conexao->beginTransaction();
            $query_novo_pessoa->execute(array($nome_paciente, $email_paciente, $telefone_paciente, $cep_paciente, $logradouro_paciente, $bairro_paciente, $cidade_paciente, $estado_estado_paciente));
            $idpessoa = $conexao->lastInsertId();
            $conexao->commit();

            echo "<p>esta é o id da ultima pessoa inserida " . $idpessoa . "</p>";
            #teste cadastro funcionario


        } catch (PDOException $erro) {
            $conexao->rollback();
            echo "erro cadastra pessoa" . $erro->getMessage();


        }
    } catch (PDOException $e) {
        echo "erro" . $e->getMessage();
    }


    #cadastrando o func
    $query_paciente = $conexao->prepare("insert into paciente(Codigo_pessoa,altura,peso,TipoSanguineo) values (?,?,?,?)");
    try {
        $conexao->beginTransaction();
        $query_paciente->execute(array($idpessoa, $altura_paciente, $paso_paciente, $tipo_sanguíneo));
        $conexao->commit();


    } catch (PDOException $erro) {
        $conexao->rollback();

    }
}
?>