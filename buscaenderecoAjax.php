<?php
header("access-control-allow-credentials: true");
header("Access-Control-Allow-Origin: null");
header('Content-Type: application/json');

require "conexao.php";
$conexao = mysqlConnect();
function filtraEntrada($dado)
{
    $dado = trim($dado);               // remove espaços no inicio e no final da string
    $dado = stripslashes($dado);       // remove contra barras: "cobra d\'agua" vira "cobra d'agua"
    $dado = htmlspecialchars($dado);   // caracteres especiais do HTML (como < e >) são codificados
    return $dado;
}

$cep_func = "";

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["cep_func"])) {
    $cep = $_POST["cep_func"];
    $cep_func = filtraEntrada($_POST["cep_func"]);
    $tabelaajax1 = $conexao->prepare("SELECT * FROM base_endereco_ajax WHERE cep = :cep LIMIT 1");
    $tabelaajax1->bindValue(':cep', $cep_func);
    $tabelaajax1->execute();
    if ($tabelaajax1->rowCount() > 0) {
        $endajax1 = $tabelaajax1->fetch(PDO::FETCH_ASSOC);
        echo json_encode($endajax1);
    } else {
        echo json_encode(array("Error" => "CEP NOT FOUND"));
    }
} else {
    echo json_encode(array("Error" => "Invalid Request"));
    http_response_code(500);
}


/*
try {
    $conexao = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $tabelaajax1 = $conexao->prepare("select * from base_endereco_ajax where cep=? limit 1");
    $tabelaajax1->execute(array($cep_func));
    if ($tabelaajax1->rowCount() > 0){
        while ($endajax1 = $tabelaajax1->fetch(PDO::FETCH_ASSOC)) {
            $dadosEndereco['Logradouro'] = $endajax1['Logradouro'];
            $dadosEndereco['Bairro']  =     $endajax1['Bairro'];
            $dadosEndereco['cidade'] =$endajax1['cidade'];
            $dadosEndereco['estado']=$endajax1['estado'];

        }
    }else{
        $dadosEndereco['Logradouro']='endereco nao encontrado';
    }
    echo json_encode($dadosEndereco);
    var_dump(json_encode($dadosEndereco));
} catch (PDOException $e) {
    echo "nao foi possivel conectar";

}
*/
