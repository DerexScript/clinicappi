<?php
require "conexao.php";
$conexao = mysqlConnect();
?>
<!doctype html>

<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="imagem/projeto-logo-clinicametica.png" type="image/x-icon">
    <title>JB Life is Gold</title>
    <link rel="stylesheet" href="styles/css_gallery.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-CuOF+2SnTUfTwSZjCXf01h7uYhfOBuxIhGKPbfEJ3+FqH/s6cIFN9bGr1HmAg4fQ" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>
<div class="container">
    <header>
        <img src="imagem/projeto-logo-clinicametica-com-rede-sociais.png" class="img-fluid"
             alt="imagem do cabebeçalho da pagina">
    </header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">JB</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#" data-tabname="home">Pagina Incial </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-tabname="photos">Fotos </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-tabname="new_address">Novo Endereço </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-tabname="appointment">Agendamento De Consulta </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#" data-tabname="logar">Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <section class="tabs">
                    <section data-tabname="home">
                        <h1>quem somos</h1>
                        <p>nossa clinica nasceu em 1950 atraves do nosso co-fundador joao batista ele sempre teve
                            consigo o desejo ajuadar o proximo</p>
                        <h1>valores</h1>
                        <p>todos nos da clinica nos importamos com o paciente acima de todo</p>
                    </section>
                    <section data-tabname="photos">
                        <h1>galeria</h1>

                        <div class="table-responsive">
                            <table class="center table" border="0" style="horizontal-align:center;">
                                <tr>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img01">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img02">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img03">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img04">
                                    </td>
                                </tr>
                                <tr>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img05">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img06">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img07">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img08">
                                    </td>
                                </tr>
                                <tr>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img09">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img10">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img11">
                                    </td>
                                    <td><img class="imgGalery" src="http://via.placeholder.com/400x300" id="img12">
                                    </td>
                                </tr>
                                <tr>
                            </table>
                        </div>


                    </section>

                    <section data-tabname="new_address">
                        <h1 class="my-3">cadastrar um novo endereço</h1>

                        <form action=" cadastra_tabela_ajax.php" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="Cep" placeholder="Cep"
                                               name="cep_novo_endereco" required>
                                        <label for="Cep" id="Cep"> Cep </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="logradouro"
                                               placeholder="logradouro" name="logradouro_novo_endereco" required>
                                        <label for="logradouro" id="logradouro"> logradouro </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <input type="text" class="form-control semborda" id="bairro"
                                               placeholder="bairro" name="bairro_novo_endereco" required>
                                        <label for="bairro" id="bairro"> bairro </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <input type="text" class="form-control semborda" id="cidade"
                                               placeholder="cidade" name="cidade_novo_endereco">
                                        <label for="cidade" id="cidade"> cidade </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <select class="form-select semborda" id="floatingSelect"
                                                aria-label="selecione um estado" required name="estado_novo_endereco">
                                            <option selected>selecione um estado</option>
                                            <option value="ac">AC</option>
                                            <option value="al">AL</option>
                                            <option value="ap">AP</option>
                                            <option value="am">AM</option>
                                            <option value="ba">BA</option>
                                            <option value="ce">CE</option>
                                            <option value="es">ES</option>
                                            <option value="go">GO</option>
                                            <option value="ma">MA</option>
                                            <option value="mt">MT</option>
                                            <option value="ms">MS</option>
                                            <option value="mg">MG</option>
                                            <option value="pa">PA</option>
                                            <option value="pb">PB</option>
                                            <option value="pr">PR</option>
                                            <option value="pe">PE</option>
                                            <option value="pi">PI</option>
                                            <option value="rj">RJ</option>
                                            <option value="rn">RN</option>
                                            <option value="rs">RS</option>
                                            <option value="ro">RO</option>

                                            <option value="rr">RR</option>
                                            <option value="sc">SC</option>
                                            <option value="sp">SP</option>
                                            <option value="se">SE</option>
                                            <option value="to">TO</option>
                                            <option value="df">DF</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 ">
                                <button class="btn btn-primary">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                         class="bi bi-plus-circle-fill" fill="currentColor"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                                    </svg>
                                    Cadastrar
                                </button>
                        </form>

                    </section>
                    <section data-tabname="appointment">
                        <h1>agendamentos</h1>
                        <form action="marcar_consulta.php" method="post">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="input_nome_Marca_consulta"
                                               placeholder="nome do paciente"
                                               name="cep_novo_endereco" required>
                                        <label for="input_nome_Marca_consulta" id="label_nome_Marca_consulta"> nome
                                            paciente </label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda"
                                               id="input_e-mail_Marca_consulta"
                                               placeholder="email do paciente" name="input_e-mail_Marca_consulta"
                                               required>
                                        <label for="input_e-mail_Marca_consulta" id="label_email_Marca_consulta"> e-mail
                                            do paciente </label>
                                    </div>
                                </div>
                            </div>
                            <?php
                            try {
                                $busca_especialidade = $conexao->prepare("select medico.Especialidade, pessoa.nome from (medico join pessoa on medico.Codigo_medico = pessoa.idpessoa);");
                                $busca_especialidade->execute();

                            } catch (PDOException $e) {
                                echo "nao foi possivel conectar";
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <select class="form-select" id="selecione_especialidade_medica"
                                                aria-label="selecione a especialidade medica" required
                                                name="selecione_especialidade_medica">


                                            <option selected>selcione a especialidade medica</option>
                                            <?php if ($busca_especialidade->rowCount() > 0) { ?>
                                                <?php while ($especialidade = $busca_especialidade->fetch(PDO::FETCH_ASSOC)) { ?>

                                                    <option value="<?php echo $especialidade["Especialidade"]; ?>"><?php echo $especialidade["Especialidade"]; ?></option>

                                                <?php } ?>

                                            <?php } ?>


                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <select class="form-select" id="selecione_nome_medico_marca_consulta"
                                                aria-label="selecione a especialidade medica" required
                                                name="selecione_nome_medico_marca_consulta">


                                            <option value="0" selected>selecione qual medico</option>


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="date" class="form-control" name="datas_consultas" id="data_consultas" disabled>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <select class="form-select" id="horas_consulta"
                                                aria-label="selecione o horario de atendimento" required
                                                name="horas_consulta">


                                        </select>
                                    </div>
                                </div>
                            </div>

                            <input type="submit" value="Agendar">

                        </form>

                    </section>


                    <section data-tabname="logar">
                        <div id="infologin"></div>
                        <h1 class="mb-3">Bem vindo digite no formulario abaixo sua crenciais para logar no sistema </h1>
                        <div id="demo"></div>
                        <form id="login_form" method="post">
                            <div class="mb-2 col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1" class="form-label">Email </label>
                                <input type="email" class="form-control semborda" id="email_login"
                                       placeholder="name@example.com" name="email_login" required>
                            </div>
                            <div class="col-md-6 mb-2 col-sm-6">
                                <label for="senha" class="form-label">senha</label>
                                <input type="password" class="form-control semborda" id="senha_login" required
                                       placeholder="digite aqui sua senha previamente cadastrada" name="senha_login">
                            </div>
                            <button type="submit" class="bnt btn-primary"> entrar</button>
                        </form>
                    </section>
                </section>


            </div>
    </main>

</div>
<script src="actions/ajax_nome_medico.js"></script>
<script src="actions/data_horario_consulta.js"></script>
<script src="actions/realiza_login_ajax.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
</script>
<script src="actions/actions_gallery.js"></script>
<script src="actions/singlepage.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-popRpmFF9JQgExhfw5tZT4I9/CI5e2QcuUZPOVXb1m7qUmeR2b50u+YFEYe1wgzy"
        crossorigin="anonymous"></script>
</body>
</html>