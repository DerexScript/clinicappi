document.querySelector("#CepC[name=cep_func]").addEventListener("blur", function (evt) {
    var formData = new FormData();
    formData.append('cep_func', evt.srcElement.value);
    var myInit = {
        method: 'POST',
        mode: 'cors',
        body: formData,
        cache: 'default'
    };
    fetch("./buscaenderecoAjax.php", myInit).then(function (response) {
        return response.text();
    }).then(function (body) {
        let resp = JSON.parse(body);
        document.querySelector("#logradouro[name=logradouro_func]").value = resp.Logradouro;
        document.querySelector("#bairro[name=bairro_func]").value = resp.Bairro;
        document.querySelector("#cidade[name=cidade_func]").value = resp.cidade;
        [...document.querySelector("#floatingSelect[name=estado_func]").options].forEach((el, ind)=>{
            if(el.innerText.toLowerCase() == resp.estado.toLowerCase()){
                document.querySelector("#floatingSelect[name=estado_func]").selectedIndex = ind;
            }
        });
    });
});

