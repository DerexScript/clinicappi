<?php

header("access-control-allow-credentials: true");
header("Access-Control-Allow-Origin: null");
header('Content-Type: application/json');

session_start();


require "conexao.php";
$conexao = mysqlConnect();
function filtraEntrada($dado)
{
    $dado = trim($dado);               // remove espaços no inicio e no final da string
    $dado = stripslashes($dado);       // remove contra barras: "cobra d\'agua" vira "cobra d'agua"
    $dado = htmlspecialchars($dado);   // caracteres especiais do HTML (como < e >) são codificados
    return $dado;
}


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["email_login"]) && isset($_POST["senha_login"])) {
    $email_login = $senha_login = "";
    $email_login = filtraEntrada($_POST["email_login"]);
    $senha_login = filtraEntrada($_POST["senha_login"]);
    $codigo_func = '';
    try {
        $buscaUsuario = $conexao->prepare("SELECT  pessoa.email,funcionario.SenhaHash,funcionario.Codigo_pessoa FROM (pessoa JOIN funcionario ON pessoa.idpessoa= funcionario.Codigo_pessoa) WHERE pessoa.email=? ");
        $buscaUsuario->execute(array($email_login));
        if ($buscaUsuario->rowCount() > 0) {
            $usuario = $buscaUsuario->fetch(PDO::FETCH_ASSOC);
            if (password_verify($senha_login, $usuario["SenhaHash"])) {
                $_SESSION["isLogin"] = "true";
                echo json_encode(array("success" => true, "msg" => "Usuario Logado Com Sucesso!"));
                $codigo_func = $usuario["Codigo_pessoa"];
                $stmt = $conexao->prepare("SELECT * FROM medico WHERE Codigo_medico = :Codigo_medico");
                $stmt->bindValue(':Codigo_medico', $codigo_func);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    $_SESSION["cargo"] = "MEDICO";
                    $stmt = null;
                } else {
                    $stmt = $conexao->prepare("SELECT * FROM funcionario WHERE Codigo_pessoa = :Codigo_pessoa");
                    $stmt->bindValue(':Codigo_pessoa', $codigo_func);
                    $stmt->execute();
                    if ($stmt->rowCount() > 0) {
                        $_SESSION["cargo"] = "FUNCIONARIO";
                        $stmt = null;
                    }else{
                        $_SESSION["cargo"] = "NULL";
                    }
                }
                //header("Location: apagina_adiministrativa.php");
            } else {
                $_SESSION["isLogin"] = "false";
                echo json_encode(array("success" => false, "msg" => "Usuario ou Senha estão incorretos..."));
            }
        }else{
            echo json_encode(array("success" => false, "msg" => "Usuario ou Senha estão incorretos..."));
        }
    } catch (PDOException $e) {
        echo "erro ao conectar-se" . $e->getMessage();
    }
}


