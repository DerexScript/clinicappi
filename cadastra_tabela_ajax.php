<?php
require "conexao.php";
$conexao=mysqlConnect();


function filtraEntrada($dado)
{
    $dado = trim($dado);               // remove espaços no inicio e no final da string
    $dado = stripslashes($dado);       // remove contra barras: "cobra d\'agua" vira "cobra d'agua"
    $dado = htmlspecialchars($dado);   // caracteres especiais do HTML (como < e >) são codificados

    return $dado;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $cep_novo_endereco = $logradouro_novo_endereco = $Bairro_novo_endereco = $cidade_novo_endereco = $estado_novo_endereco = "";

    $cep_novo_endereco = filtraEntrada($_POST["cep_novo_endereco"]);
    $logradouro_novo_endereco = filtraEntrada($_POST["logradouro_novo_endereco"]);
    $Bairro_novo_endereco = filtraEntrada($_POST["bairro_novo_endereco"]);
    $cidade_novo_endereco = filtraEntrada($_POST["cidade_novo_endereco"]);
    $estado_novo_endereco = filtraEntrada($_POST["estado_novo_endereco"]);


    try {

        $query_novo_endereco = $conexao->prepare("insert into base_endereco_ajax(cep,Logradouro,Bairro,cidade,estado) values (?,?,?,?,?)");
        try {
            $conexao->beginTransaction();
            $query_novo_endereco->execute(array($cep_novo_endereco, $logradouro_novo_endereco, $Bairro_novo_endereco, $cidade_novo_endereco, $estado_novo_endereco));
            $conexao->commit();
            print  $conexao->lastInsertId();

        } catch (PDOException $erro) {
            $conexao->rollback();
            echo "erro cadastra endereco" . $erro->getMessage();


        }
    } catch (PDOException $e) {
        echo "erro" . $e->getMessage();
    }
}
?>