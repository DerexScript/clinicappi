<?php
session_start();
if (isset($_SESSION["isLogin"]) && $_SESSION["cargo"]) {
    unset($_SESSION["isLogin"]);
    unset($_SESSION["cargo"]);
    header("Location: ./");
}
?>
