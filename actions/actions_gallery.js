$(document).ready(function () {

    $(".imgGalery").each(function (i) {
        $(this).delay(200 * i).fadeIn();
    });

    $(".imgGalery").hover(
        function () {
            $(this).animate({
                width: '280px',
                height: '250px'
            });
        },

        function () {
            $(this).animate({
                width: '230px',
                height: '200px'
            });
        }
    );

});
