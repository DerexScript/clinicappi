window.onload = function () {
    let buttons = document.querySelectorAll('nav ul li a') //pego todos os buttons da pagina
    for (let button of buttons) {
        button.onclick = () => openTab(button.dataset.tabname) //pecurro o'array de botoes' e //coloco evento de clique a cada button chamando uma func openTab passando o //atributo tabname

    }
    openTab('cadastro_funcionarios') //pag a ser carregado primeiro


}

function openTab(tabname) {
    const lastTabActive = document.querySelector('.tabActive')
    if (lastTabActive !== null) {
        lastTabActive.className = ""
    }

    const lastButtonActive = document.querySelector('.buttonActive')
    if (lastButtonActive !== null) {
        lastButtonActive.className = ""

    }
    //seleciono  a atraves de uma string css
    const query1 = ".tabs>section[data-tabname='" + tabname + "']"
    const query2 = "nav ul li a[data-tabname='" + tabname + "']"
    document.querySelector(query1).className = "tabActive"
    document.querySelector(query2).className = "buttonActive"

}
