document.querySelector("#login_form").addEventListener("submit", function(evt){
    evt.preventDefault();
    let email_login = document.querySelector("#email_login");
    let senha_login = document.querySelector("#senha_login");
    if(email_login.value.length > 0 && senha_login.value.length > 0){
        var formData = new FormData();
        formData.append('email_login', email_login.value);
        formData.append('senha_login', senha_login.value);
        var myInit = {
            method: 'POST',
            mode: 'cors',
            body: formData,
            cache: 'default'
        };
        fetch("./realiza_login.php", myInit).then(function (response) {
            return response.text();
        }).then(function (body) {
            let responseData = JSON.parse(body);
            if(responseData.success == true){
                alert(responseData.msg);
                window.location.href = "./apagina_adiministrativa.php";
            }else{
                alert(responseData.msg);
            }
        });
    }else{
        alert("Preencha Os Campos De Login E senha....");
    }
});