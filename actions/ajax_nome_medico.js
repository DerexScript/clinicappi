document.querySelector("#selecione_especialidade_medica").addEventListener("change", function (evt) {
    var formData = new FormData();
    formData.append('selecione_especialidade_medica', evt.srcElement.value);
    var myInit = {
        method: 'POST',
        mode: 'cors',
        body: formData,
        cache: 'default'
    };
    fetch("./trazer_nome_medico.php", myInit).then(function (response) {
        return response.text();
    }).then(function (body) {
        let resp = JSON.parse(body);
        let newOpt;
        document.querySelector("#selecione_nome_medico_marca_consulta[name=selecione_nome_medico_marca_consulta]").innerHTML = `<option value="0" selected="">selecione qual medico</option>`
        resp.forEach(function (el, ind) {
            newOpt = document.createElement("option");
            newOpt.value = el.idpessoa;
            newOpt.innerText = el.nome;
            document.querySelector("#selecione_nome_medico_marca_consulta[name=selecione_nome_medico_marca_consulta]").appendChild(newOpt);
        });
    });
});
