document.querySelector("#cep_pac_input").addEventListener("blur", function (evt) {
    var formData = new FormData();
    formData.append('cep_paciente', evt.srcElement.value);
    var myInit = {
        method: 'POST',
        mode: 'cors',
        body: formData,
        cache: 'default'
    };
    fetch("./busca_cep_paciente.php", myInit).then(function (response) {
        return response.text();
    }).then(function (body) {
        let resp = JSON.parse(body);
        document.querySelector("#logradouro[name=logradouro_paciente]").value = resp.Logradouro;
        document.querySelector("#bairro[name=bairro_paciente]").value = resp.Bairro;
        document.querySelector("#cidade[name=cidade_paciente]").value = resp.cidade;
        [...document.querySelector("#floatingSelect[name=estado_estado_paciente]").options].forEach((el, ind) => {
            if (el.innerText.toLowerCase() == resp.estado.toLowerCase()) {
                document.querySelector("#floatingSelect[name=estado_estado_paciente]").selectedIndex = ind;
            }
        });
    });
});
