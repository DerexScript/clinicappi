document.querySelector("#selecione_nome_medico_marca_consulta").addEventListener("change", evt => {
    if (evt.srcElement.value != 0) {
        document.querySelector("#data_consultas").removeAttribute("disabled");
    }
});

document.querySelector("#data_consultas").addEventListener("change", function (evt) {
    //evt.srcElement.value
    let idMedico = document.querySelector("#selecione_nome_medico_marca_consulta").value;
    let chosenDate = evt.srcElement.value;


    var formData = new FormData();
    formData.append('idMedico', idMedico);
    formData.append('chosenDate', chosenDate);

    var myInit = {
        method: 'POST',
        mode: 'cors',
        body: formData,
        cache: 'default'
    };
    fetch("./data_horario_consulta.php", myInit).then(function (response) {
        return response.text();
    }).then(function (body) {
        let resp = JSON.parse(body);
        let hoursArray = [];
        if (resp.success == true) {
            for (let i = 0; i < Object.values(resp).length; i++) {
                if (Object.values(resp)[i] != true) {
                    hoursArray.push(Object.values(resp)[i].hour);
                }
            }
            let newOpt;
            document.querySelector("#horas_consulta").innerHTML = ``;
            for (let i = 8; i <= 17; i++) {
                if (!hoursArray.includes(`${i.toString().padStart(2, '0')}:00:00`)) {
                    newOpt = document.createElement("option");
                    newOpt.value = `${i.toString().padStart(2, '0')}:00:00`;
                    newOpt.innerText = `${i.toString().padStart(2, '0')}:00:00`;
                    document.querySelector("#horas_consulta").appendChild(newOpt);
                }
            }
        } else {
            document.querySelector("#horas_consulta").innerHTML = ``;
            for (let i = 8; i <= 17; i++) {
                newOpt = document.createElement("option");
                newOpt.value = `${i.toString().padStart(2, '0')}:00:00`;
                newOpt.innerText = `${i.toString().padStart(2, '0')}:00:00`;
                document.querySelector("#horas_consulta").appendChild(newOpt);
            }
        }
    });


});