<?php
session_start();
if (!isset($_SESSION["isLogin"]) || $_SESSION["isLogin"] == false) {
    echo "Acesso Negado!";
    header("Refresh:2; url=./", true, 303);
    exit();
}
require "conexao.php";
$conexao = mysqlConnect();
?>

<!doctype html>
<html lang="pt-br" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="imagem/projeto-logo-clinicameticalogin.png" type="image/x-icon">
    <title>JB Life is Gold</title>
    <link rel="stylesheet" href="styles/cadastro_medico.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-CuOF+2SnTUfTwSZjCXf01h7uYhfOBuxIhGKPbfEJ3+FqH/s6cIFN9bGr1HmAg4fQ" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>
<div class="container">
    <header>
        <img src="imagem/projeto-logo-clinicameticalogin.png" class="img-fluid"
             alt="imagem do cabebeçalho da pagina">
    </header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">JB</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#" data-tabname="cadastro_funcionarios">cadastro
                            cadastro de funcionario </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-tabname="cadastro_paciente">cadastro de pacientes </a>
                    </li>
                    <!--menu dropdown-->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-expanded="false">
                            Listagens
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#" data-tabname="listagem_func">Listagem dos funcionários
                                    cadastrados</a></li>
                            <li><a class="dropdown-item" href="# " data-tabname="listagem_paciente"> Listagem dos
                                    pacientes cadastrados</a></li>
                            <li><a class="dropdown-item" href="#" data-tabname="endereco_ajax"> Listagem dos endereços
                                    auxiliares AJAX cadastrados</a></li>
                            <?php if ($_SESSION["cargo"] == "MEDICO"): ?>
                                <li><a class="dropdown-item " id="menucasosejamedico" href="#"
                                       data-tabname="listagem_consultas_medico"> Listagem dos
                                        agendamentos de consultas apenas do funcionário logado caso ele seja um
                                        médico.</a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="logout.php" class="btn btn-outline-danger">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main>
        <div class="row">
            <div class="col-sm-12 col-md-12">

                <section class="tabs">
                    <section data-tabname="cadastro_funcionarios">
                        <h1>cadastro de funcionario</h1>
                        <form action="cadastro_funcionario.php" method="POST" id="cadastra_func"
                              name="form_cadastra_func">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="nome" name="nome_func"
                                               placeholder="nome do funcionario" required>
                                        <label for="nome" id="nome"> nome </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="email" class="form-control semborda" id="email" placeholder="email"
                                               name="email_func" required>
                                        <label for="email" id="email"> email </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="telefone"
                                               placeholder="telefone" name="telefone_func" required>
                                        <label for="telefone" id="telefone"> telefone </label>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="CepC" placeholder="Cep"
                                               name="cep_func" required>
                                        <label for="Cep" id="Cep"> Cep </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="logradouro"
                                               placeholder="logradouro" name="logradouro_func" required>
                                        <label for="logradouro" id="logradouro"> logradouro </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <input type="text" class="form-control semborda" id="bairro"
                                               placeholder="bairro" name="bairro_func" required>
                                        <label for="bairro" id="bairro"> bairro </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <input type="text" class="form-control semborda" id="cidade"
                                               placeholder="cidade" name="cidade_func" required>
                                        <label for="cidade" id="cidade"> cidade </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <select class="form-select" id="floatingSelect"
                                                aria-label="selecione um estado" name="estado_func" required>
                                            <option selected>selecione um estado</option>
                                            <option value="ac">AC</option>
                                            <option value="al">AL</option>
                                            <option value="ap">AP</option>
                                            <option value="am">AM</option>
                                            <option value="ba">BA</option>
                                            <option value="ce">CE</option>
                                            <option value="es">ES</option>
                                            <option value="go">GO</option>
                                            <option value="ma">MA</option>
                                            <option value="mt">MT</option>
                                            <option value="ms">MS</option>
                                            <option value="mg">MG</option>
                                            <option value="pa">PA</option>
                                            <option value="pb">PB</option>
                                            <option value="pr">PR</option>
                                            <option value="pe">PE</option>
                                            <option value="pi">PI</option>
                                            <option value="rj">RJ</option>
                                            <option value="rn">RN</option>
                                            <option value="rs">RS</option>
                                            <option value="ro">RO</option>

                                            <option value="rr">RR</option>
                                            <option value="sc">SC</option>
                                            <option value="sp">SP</option>
                                            <option value="se">SE</option>
                                            <option value="to">TO</option>
                                            <option value="df">DF</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating">
                                    <input type="date" class="form-control semborda" id="datainicio"
                                           placeholder="data de contrato" name="data_inicio_func" required>
                                    <label for="datainicio" id="datainicio"> data de contrato </label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-floating">
                                    <input type="number" step="0.010" name="salario_func" class="form-control semborda"
                                           id="salario_f"
                                           placeholder="salario" required>
                                    <label for="salario" id="salario"> salario </label>
                                </div>
                            </div>
                            <div class="col-md-4 gy-3 mb-4">
                                <div class="form-floating">
                                    <input type="password" step="" name="passfunc" class="form-control semborda"
                                           id="passfunc_f"
                                           placeholder="senha" required>
                                    <label for="passfunc" id="passfunc"> senha </label>
                                </div>
                            </div>


                            <div class="row gy-3">
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <select class="form-select" name="tipofunc" id="tipofunc" required>

                                            <option selected> tipo funcionario</option>
                                            <option value="func_medico"> medico</option>
                                            <option value="func_comum"> Cumum</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row my-3 gy-3">
                                    <div class="col-md-4">
                                        <div class="form-floating escondercampo">
                                            <input type="text" class="form-control  semborda" id="crm"
                                                   placeholder="crm" name="crm_medico">
                                            <label for="crm" id="crm"> crm </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-floating escondercampo">
                                            <input type="text" class="form-control campo_oculto semborda"
                                                   id="especialidade" name="especialidade_medico"
                                                   placeholder="especialidade">
                                            <label for="especialidade" id="especialidade"> especialidade </label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 ">
                                        <button class="btn btn-primary">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16"
                                                 class="bi bi-plus-circle-fill" fill="currentColor"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                                            </svg>
                                            Cadastrar
                                        </button>
                        </form>


                    </section>
                    <section data-tabname="cadastro_paciente">
                        <h1>cadastro de pacientes</h1>

                        <form action="cadastro_pacientes.php" method="post">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="nome" name="nome_paciente"
                                               placeholder="nome do do paciente" required>
                                        <label for="nome" id="nome"> nome paciente </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="email" class="form-control semborda" id="email" placeholder="email"
                                               name="email_paciente" required>
                                        <label for="email" id="email"> email paciente </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="telefone"
                                               placeholder="telefone" name="telefone_paciente" required>
                                        <label for="telefone" id="telefone"> telefone </label>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="cep_pac_input"
                                               placeholder="Cep"
                                               name="cep_paciente"
                                               required>
                                        <label for="Cep" id="Cep"> Cep </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="logradouro"
                                               placeholder="logradouro" name="logradouro_paciente" required>
                                        <label for="logradouro" id="logradouro"> logradouro </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <input type="text" class="form-control semborda" id="bairro"
                                               placeholder="bairro" name="bairro_paciente" requirede>
                                        <label for="bairro" id="bairro"> bairro </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <input type="text" class="form-control semborda" id="cidade"
                                               placeholder="cidade" name="cidade_paciente" required>
                                        <label for="cidade" id="cidade"> cidade </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <select class="form-select" id="floatingSelect"
                                                aria-label="selecione um estado" name="estado_estado_paciente" required>
                                            <option selected>selecione um estado</option>
                                            <option value="ac">AC</option>
                                            <option value="al">AL</option>
                                            <option value="ap">AP</option>
                                            <option value="am">AM</option>
                                            <option value="ba">BA</option>
                                            <option value="ce">CE</option>
                                            <option value="es">ES</option>
                                            <option value="go">GO</option>
                                            <option value="ma">MA</option>
                                            <option value="mt">MT</option>
                                            <option value="ms">MS</option>
                                            <option value="mg">MG</option>
                                            <option value="pa">PA</option>
                                            <option value="pb">PB</option>
                                            <option value="pr">PR</option>
                                            <option value="pe">PE</option>
                                            <option value="pi">PI</option>
                                            <option value="rj">RJ</option>
                                            <option value="rn">RN</option>
                                            <option value="rs">RS</option>
                                            <option value="ro">RO</option>

                                            <option value="rr">RR</option>
                                            <option value="sc">SC</option>
                                            <option value="sp">SP</option>
                                            <option value="se">SE</option>
                                            <option value="to">TO</option>
                                            <option value="df">DF</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="email" class="form-control semborda" id="paso_paciente"
                                               placeholder="tipo sanguíneo paciente" name="tipo_sanguíneo" required>
                                        <label for="sanguineo" id="sanguineo"> tipo sanguineo do paciente </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="email" class="form-control semborda" id="paso_paciente"
                                               placeholder="paso paciente" name="paso_paciente" required>
                                        <label for="pesp" id="peso"> peso do paciente </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-floating mb-2">
                                        <input type="text" class="form-control semborda" id="altura_paciente"
                                               placeholder="altura sanguíneo" name="altura_paciente" required>
                                        <label for="altura" id="altura"> altura paciente </label>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12 ">
                                <button class="btn btn-primary">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                         class="bi bi-plus-circle-fill" fill="currentColor"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                                    </svg>
                                    Cadastrar
                                </button>
                        </form>

                    </section>


                    <section data-tabname="listagem_func">
                        <h1>listagem de funcionario</h1>

                        <div class="form-check form-switch">
                            <input class="form-check-input check_funcionario" type="checkbox" id="check_funcionario"
                                   value="sim">
                            <label class="form-check-label" for="check_funcionario">Visualizar a tabela so com
                                html</label>
                        </div>


                        <?php

                        try {
                            $busca_todos_funcionarios = $conexao->prepare("select  pessoa.nome,pessoa.email,pessoa.telefone,pessoa.cep,pessoa.Logradouro,pessoa.Bairro,pessoa.cidade,pessoa.estado,funcionario.DataContrato, funcionario.salario from (pessoa join funcionario on pessoa.idpessoa= funcionario.Codigo_pessoa)");
                            $busca_todos_funcionarios->execute();
                            echo "<p>listando</p>.<br>";

                        } catch (PDOException $e) {
                            echo "nao foi possivel conectar";
                        }
                        ?>
                        <table class="tabfunc_funcionario_html" border="1">


                            <!--transendo os dados-->
                            <thead>
                            <tr>
                                <th>nome do funcionario</th>
                                <th>email funcionario</th>
                                <th>telefone funcionario</th>
                                <th>cep funcionario</th>
                                <th>lougradouro funcionario</th>
                                <th>Bairro funcionario</th>
                                <th>Cidade funcionario</th>
                                <th>Estado Funcionario</th>
                                <th>data de inicio (cadastro)</th>
                                <th>salario</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($busca_todos_funcionarios->rowCount() > 0) { ?>
                                <?php while ($funcs = $busca_todos_funcionarios->fetch(PDO::FETCH_ASSOC)) { ?>

                                    <tr>
                                        <td><?php echo $funcs["nome"]; ?></td>
                                        <td><?php echo $funcs["email"]; ?></td>
                                        <td><?php echo $funcs["telefone"]; ?></td>
                                        <td><?php echo $funcs["cep"]; ?></td>
                                        <td><?php echo $funcs["Logradouro"]; ?></td>
                                        <td><?php echo $funcs["Bairro"]; ?></td>
                                        <td><?php echo $funcs["cidade"]; ?></td>
                                        <td><?php echo $funcs["estado"]; ?></td>
                                        <td><?php echo date('d/m/Y', strtotime($funcs["DataContrato"])); ?></td>
                                        <td><?php echo $funcs["salario"]; ?></td>


                                    </tr>
                                <?php } ?>

                            <?php } ?>


                            </tbody>

                        </table>
                        <?php
                        try {
                            $busca_todos_funcionarios_bot = $conexao->prepare("select  pessoa.nome,pessoa.email,pessoa.telefone,pessoa.cep,pessoa.Logradouro,pessoa.Bairro,pessoa.cidade,pessoa.estado,funcionario.DataContrato, funcionario.salario from (pessoa join funcionario on pessoa.idpessoa= funcionario.Codigo_pessoa)");
                            $busca_todos_funcionarios_bot->execute();

                        } catch (PDOException $e) {
                            echo "nao foi possivel conectar";
                        }
                        ?>
                        <table class="table table-striped table-hover tabfunc_funcionario_botstrap">
                            <tr>

                            </tr>
                            </thead>
                            <th>nome do funcionario</th>
                            <th>email funcionario</th>
                            <th>telefone funcionario</th>
                            <th>cep funcionario</th>
                            <th>lougradouro funcionario</th>
                            <th>Bairro funcionario</th>
                            <th>Cidade funcionario</th>
                            <th>Estado Funcionario</th>
                            <th>data de inicio (cadastro)</th>
                            <th>salario</th>
                            <tbody>
                            <?php if ($busca_todos_funcionarios_bot->rowCount() > 0) { ?>
                                <?php while ($funcs = $busca_todos_funcionarios_bot->fetch(PDO::FETCH_ASSOC)) { ?>

                                    <tr>
                                        <td><?php echo $funcs["nome"]; ?></td>
                                        <td><?php echo $funcs["email"]; ?></td>
                                        <td><?php echo $funcs["telefone"]; ?></td>
                                        <td><?php echo $funcs["cep"]; ?></td>
                                        <td><?php echo $funcs["Logradouro"]; ?></td>
                                        <td><?php echo $funcs["Bairro"]; ?></td>
                                        <td><?php echo $funcs["cidade"]; ?></td>
                                        <td><?php echo $funcs["estado"]; ?></td>
                                        <td><?php echo date('d/m/Y', strtotime($funcs["DataContrato"])); ?></td>
                                        <td><?php echo $funcs["salario"]; ?></td>


                                    </tr>
                                <?php } ?>

                            <?php } ?>
                            </tbody>
                        </table>
                    </section>


                    <section data-tabname="listagem_paciente">
                        <h1>listagem de paciente</h1>

                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" id="check_paciente" value="sim">
                            <label class="form-check-label" for="flexSwitchCheckChecked">Visualizar a tabela so com
                                html</label>
                        </div>

                        <!-- listar pacientes-->

                        <?php


                        try {
                            $pacientes = $conexao->prepare("select  pessoa.nome,pessoa.email,pessoa.telefone,pessoa.cep,pessoa.Logradouro,pessoa.Bairro,pessoa.cidade,pessoa.estado, paciente.altura,paciente.peso,paciente.TipoSanguineo from (pessoa join paciente on pessoa.idpessoa= paciente.Codigo_pessoa)");
                            $pacientes->execute();
                            echo "<p>listando</p>.<br>";

                        } catch (PDOException $e) {
                            echo "nao foi possivel conectar";
                        }
                        ?>
                        <table class="table_paciente_html" border="1">


                            <!--transendo os dados-->
                            <thead>
                            <tr>
                                <th>nome do paciente</th>
                                <th>email paciente</th>
                                <th>telefone paciente</th>
                                <th>cep paciente</th>
                                <th>lougradouro paciente</th>
                                <th>Bairro bairro</th>
                                <th>Cidade paciente</th>
                                <th>Estado paciente</th>
                                <th>altura</th>
                                <th>peso</th>
                                <th>Tipo Sanguineo</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($pacientes->rowCount() > 0) { ?>
                                <?php while ($paci = $pacientes->fetch(PDO::FETCH_ASSOC)) { ?>

                                    <tr>
                                        <td><?php echo $paci["nome"]; ?></td>
                                        <td><?php echo $paci["email"]; ?></td>
                                        <td><?php echo $paci["telefone"]; ?></td>
                                        <td><?php echo $paci["cep"]; ?></td>
                                        <td><?php echo $paci["Logradouro"]; ?></td>
                                        <td><?php echo $paci["Bairro"]; ?></td>
                                        <td><?php echo $paci["cidade"]; ?></td>
                                        <td><?php echo $paci["estado"]; ?></td>
                                        <td><?php echo $paci["altura"]; ?></td>
                                        <td><?php echo $paci["peso"]; ?></td>
                                        <td><?php echo $paci["TipoSanguineo"]; ?></td>


                                    </tr>
                                <?php } ?>

                            <?php } ?>


                            </tbody>

                        </table>
                        <?php
                        try {
                            $busca_todos_paciente_bot = $conexao->prepare("select  pessoa.nome,pessoa.email,pessoa.telefone,pessoa.cep,pessoa.Logradouro,pessoa.Bairro,pessoa.cidade,pessoa.estado, paciente.altura,paciente.peso,paciente.TipoSanguineo from (pessoa join paciente on pessoa.idpessoa= paciente.Codigo_pessoa)");
                            $busca_todos_paciente_bot->execute();

                        } catch (PDOException $e) {
                            echo "nao foi possivel conectar";
                        }
                        ?>
                        <table class="table table-striped table-hover table_paciente_botstrap">
                            <tr>

                            </tr>
                            </thead>
                            <th>nome do paciente</th>
                            <th>email paciente</th>
                            <th>telefone paciente</th>
                            <th>cep paciente</th>
                            <th>lougradouro paciente</th>
                            <th>Bairro bairro</th>
                            <th>Cidade paciente</th>
                            <th>Estado paciente</th>
                            <th>altura</th>
                            <th>peso</th>
                            <th>Tipo Sanguineo</th>
                            <tbody>
                            <?php if ($busca_todos_paciente_bot->rowCount() > 0) { ?>
                                <?php while ($paci = $busca_todos_paciente_bot->fetch(PDO::FETCH_ASSOC)) { ?>

                                    <tr>
                                        <td><?php echo $paci["nome"]; ?></td>
                                        <td><?php echo $paci["email"]; ?></td>
                                        <td><?php echo $paci["telefone"]; ?></td>
                                        <td><?php echo $paci["cep"]; ?></td>
                                        <td><?php echo $paci["Logradouro"]; ?></td>
                                        <td><?php echo $paci["Bairro"]; ?></td>
                                        <td><?php echo $paci["cidade"]; ?></td>
                                        <td><?php echo $paci["estado"]; ?></td>
                                        <td><?php echo $paci["altura"]; ?></td>
                                        <td><?php echo $paci["peso"]; ?></td>
                                        <td><?php echo $paci["TipoSanguineo"]; ?></td>


                                    </tr>
                                <?php } ?>

                            <?php } ?>
                            </tbody>
                        </table>


                    </section>
                    <section data-tabname="endereco_ajax">
                        <h1>listagem de endereço ajax</h1>

                        <!--endereços tabelas ajax-->

                        <div class="form-check form-switch">
                            <input class="form-check-input check_funcionario" type="checkbox" id="check_endeajax"
                                   value="sim">
                            <label class="form-check-label" for="check_endeajax">Visualizar a tabela so com
                                html</label>
                        </div>


                        <?php

                        $servername = "localhost";
                        $username = "root";
                        $password = "";
                        $dbname = "clinica_ppi";

                        try {
                            $tabelaajax1 = $conexao->prepare("select * from base_endereco_ajax");
                            $tabelaajax1->execute();
                            echo "<p>listando</p>.<br>";

                        } catch (PDOException $e) {
                            echo "nao foi possivel conectar";
                        }
                        ?>
                        <table class="table_endajax_html" border="1">


                            <!--transendo os dados-->
                            <thead>
                            <tr>
                                <th>cep</th>
                                <th>Logradouro</th>
                                <th>Bairro</th>
                                <th>cidade</th>
                                <th>estado</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($tabelaajax1->rowCount() > 0) { ?>
                                <?php while ($endajax1 = $tabelaajax1->fetch(PDO::FETCH_ASSOC)) { ?>

                                    <tr>

                                        <td><?php echo $endajax1["cep"]; ?></td>
                                        <td><?php echo $endajax1["Logradouro"]; ?></td>
                                        <td><?php echo $endajax1["Bairro"]; ?></td>
                                        <td><?php echo $endajax1["cidade"]; ?></td>
                                        <td><?php echo $endajax1["estado"]; ?></td>


                                    </tr>
                                <?php } ?>

                            <?php } ?>


                            </tbody>

                        </table>
                        <?php
                        try {
                            $tabelaajax2 = $conexao->prepare("select * from base_endereco_ajax");
                            $tabelaajax2->execute();

                        } catch (PDOException $e) {
                            echo "nao foi possivel conectar";
                        }
                        ?>
                        <table class="table table-striped table-hover table_endajax_bootstrap">
                            <tr>

                            </tr>
                            </thead>
                            <th>cep</th>
                            <th>Logradouro</th>
                            <th>Bairro</th>
                            <th>cidade</th>
                            <th>estado</th>
                            <tbody>
                            <?php if ($tabelaajax2->rowCount() > 0) { ?>
                                <?php while ($endajax2 = $tabelaajax2->fetch(PDO::FETCH_ASSOC)) { ?>

                                    <tr>

                                        <td><?php echo $endajax2["cep"]; ?></td>
                                        <td><?php echo $endajax2["Logradouro"]; ?></td>
                                        <td><?php echo $endajax2["Bairro"]; ?></td>
                                        <td><?php echo $endajax2["cidade"]; ?></td>
                                        <td><?php echo $endajax2["estado"]; ?></td>


                                    </tr>
                                <?php } ?>

                            <?php } ?>
                            </tbody>
                        </table>


                    </section>
                    <section data-tabname="listagem_consultas_medico">
                        <h1>listagem de consultas de medicos</h1>
                    </section>

                </section>


            </div>
    </main>

</div>
<script src="actions/modicatabela.js"></script>
<script src="actions/jquery/jquery.js"></script>
<script src="actions/busca_endereco_pacienteajax.js"></script>
<script src="busca_endereco_funcionario.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script src="actions/actions_gallery.js"></script>
<script src="actions/singlepage_admin.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-popRpmFF9JQgExhfw5tZT4I9/CI5e2QcuUZPOVXb1m7qUmeR2b50u+YFEYe1wgzy"
        crossorigin="anonymous"></script>
<script src="actions/cadastro_medico.js"></script>
</body>
</html>